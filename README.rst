================================================================
 Online Monitor for the Pandas Dataframe Transformations
================================================================





:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Sep 24, 2015, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.

.. admonition:: Dedication

    For python users & co-developers.

.. admonition:: Abstract

    This  project illustrates the practical steps of the making online monitor to
    trace the status of the long-time transformations of the pandas dataframe.

.. meta::
   :keywords: pandas,ipython,dataframe,javascript,flask,socket.io
   :description lang=en: A demonstration of the reStructuredText
       markup language, containing examples of all basic
       constructs and many advanced constructs.




.. contents:: Table of Contents





----------------------------------------------------------------
Introduction to the topic and motivation
----------------------------------------------------------------

I want to introduce you to my motivation which forces me to develop this project.
The main part of any statistical inference is preparation of data to consumable 
format and generation of new data on the basis of existing information. 
For ``pythonic`` statisticians, this means that 

1. a pandas dataframe has to be built from the database or file

2. new data should be inserted into the dataframe

3. the dataframe should be transformed (normalized)


All steps usually take much time if the number of entries is large.

If you run your analysis on the desktop, you can logout the trace of your computations to the 
display via calls of ``sys.stdout(...) or print(...)`` .
In the case when your code is running online in a cloud or on some remote server,
such approach will be problematic. Also the logging of your calculations to files remotely, 
can not serve your intentions  if you don't have a ``ssh`` tunnel to the remote server.





Installation of the package
======================================

.. Important::
  
    The project relies on the presence of ``Autotools`` in the system.
    Before, proceed further, please, install them: ``sudo apt-get install autotools-dev``.

Simply, clone the project 

.. code-block:: bash

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/pandas-dataframe-transformation-monitor.git

and test a configuration of your system that you have all components installed:
    
* python 

* scientific python modules.

In order to do this, you can just run the default rule of  ``Makefile`` via the 
command in the shell:

.. code-block:: bash
    
    make
    
    
----------------------------------------------------------------
Practical examples 
----------------------------------------------------------------

There is a list of exercises that are provided by the package.
You can:
    
* start and test a web-socket server
* start and test a ``socket.io`` server
* monitor the progress of the dataframe transformation (what was the aim of this work)


Test of the web-socket server
===================================

A simple python server utilizing the HTML5 Web Socket is ``bin/_websocketserver_example.py``.

You can open a terminal and type


.. code-block:: bash

    python bin/_websocketserver_example.py
    

if you open another terminal and type

.. code-block:: bash

    firefox templates/example_websocket.html

you will see a ``Pong`` response from the server. This example shows you a very premitive web socket communication.


Test of the ``socket.io`` server
======================================================================

This is an example of how one can use the flask-based server with the ``socket.io``
support. 

A simple flask server utilizing the  `socket.io`` is ``bin/_monitorserver_example.py``.
As in the example above, you open two terminals and run the commands


.. code-block:: bash

    .. terminal 1
    python bin/_monitorserver_example.py

    .. terminal 2
    firefox templates/example_monitor_socketio.html

As a reult, you will see the log of communications (in the browser window) between the firefox and server.


Online Monitor of the progress for the dataframe transformation 
===================================================================

The last and most important exercises is about monitoring the process of the dataframe transformation.
I provide two ``socket.io`` servers  which performs the ``apply`` method to the pandas dataframe in background.

One server, which is called ``bin/monitorserver.py``,  performs single transformation of the dataframe, another one,
``bin/monitorserver_multi_transform.py`` applies three different functions to generate new data to be inserted into the 
dataframe.


``blinker`` signals
-----------------------------------

The communication between the process of transformation and server goes through the ``blinker`` signals.
There are several types of the signals:

* ``on_ready``, which is sent when the Processor making transformation is ready to start
* ``on_current_process``, which is sent when the Processor is making application of the transformation function to the current row in the dataframe
* ``on_total_process``, which is sent when the Processor has finished the current transformation and  is going to start new one taken the next transformation from the list
* ``on_complete``, which is sent when the Processor has finished the last transformation from the list

The signals are not only used as indicators of the some hapeness in the system, but they also deliver different
information coded as  python objects from the Processor to the server. 
Each signal is processed by the corresponded reciver method on the server side (foreground process). 
When recivers  catch the signals, they emit  ``socket.io``  messages of particular types  with progress status and 
numerical data to the Javascript (JS) client of the socket.io tunnel. The JS client assign a callback method to each type 
of the messages in order to redraw DOM of the html page served for monitoring. In such way, ``blinker`` signals and 
``socket.io`` messages are combined to link the python functions making transformations of the pandas dataframe with
the HTML elements graphically representing changes in the dataframe.


Schematic View of the workflow
-----------------------------------


.. figure:: figs/Online_monitor_Pandas_dataframe.png
   :alt: Schematic View of the workflow
   
   Schematic View of the workflow


Daemons
----------------------------------------

In order to start the python servers as daemons without using /etc/init.d and other dedicated stuff,
I have added the simple framework, which allows to run any executable as a daemon. This includes
the files

* ``bin/setup.daemon.sh``
* ``bin/config.ini``
* ``bin/daemon.sh``
* ``bin/_oobash.sh``
* ``bin/respawn_double_fork.sh``

The ``bin/setup.daemon.sh`` defines properties  of the daemons:

* DAEMON NAME -- a unique string to identify daemon settings stored in the ``bin/config.ini``
* DAEMON UUID -- a unique 8-letters string to identify daemon process in the system. This
  uuid is used to check and kill the process in the system
* DAEMON CMD -- a command line used by the daemon to start the executable 

The  oo-style framework  ``OOBash``  is used to handle these properties as attributes of the object
``DAEMON`` in OOP style.

The ``bin/daemon.sh`` is a control script to start/stop/check_status of the daemon. It starts internally 
the script ``bin/respawn_double_fork.sh`` as a double-forked process which is respawned, i.e.
``( ${RESPAWNSCRIPT} \"${DAEMON_CMD}\" 1 \"\\--${DAEMON_UUID}\" \"${KILLWORD}\" & ) &``.
What doest it mean ``DOUBLE-FORKED and RESPAWNED``? ``DOUBLE-FORKED`` means that it becomes the child of the init process. 
Because it is ``RESPAWNED``, i.e. it has an internal loop, it never dies!
``bin/respawn_double_fork.sh`` uses  ``netcat`` to listen the port 9760 on the localhost.
The only one way to stop ``${RESPAWNSCRIPT}`` is to  send the message  via netcat:

.. code-block:: 

    echo -ne ${KILLWORD} | nc -q1  localhost 9760


That is what ``bin/daemon.sh stop`` makes.

So, in order, to start, for example, ``bin/monitorserver_multi_transform.py`` as a daemon, one can do

.. code-block:: 
    
   cd bin;
   ./setup.daemons.sh;
   export DAEMON_NAME=MONITORSOCKETIO_MULTI_TRANSFORM; 
   ./daemon.sh start;
    
    
    
Test of the Online Monitor
-----------------------------------------

Ok. Now, we are ready to play with two servers:

* ``bin/monitorserver.py``
* ``bin/monitorserver_multi_transform.py``

The first makes only one transformation of the pandas dataframe. The second one performs a set
of transformations.

If you start the ``bin/monitorserver.py``

.. code-block:: 
    
   cd bin;
   ./setup.daemons.sh;
   export DAEMON_NAME=MONITORSOCKETIO; 
   ./daemon.sh start;

and open the template ``monitor_socket.io`` in the ``firefox``

.. code-block:: bash
    
    firefox templates/monitor_socket.io


you will see something like this



.. figure:: screencast2gif/GIFrecord_2015-10-02_134752.gif
   :alt: Demonstration of the online Monitor 
   
   Demonstration of the online Monitor 



In analogy with the ``bin/monitorserver.py``, the test of the ``bin/monitorserver_multi_transform.py``
is 


.. code-block:: 
    
   cd bin;
   ./setup.daemons.sh;
   export DAEMON_NAME=MONITORSOCKETIO_MULTI_TRANSFORM;   
   ./daemon.sh start;
   cd -
   firefox templates/monitor_multi_transform_socketio.html


The web page will be rendered with statuses of the transformations in the real time.



.. figure:: screencast2gif/GIFrecord_2015-10-02_140708.gif
   :alt: Demonstration of the online Monitor 
   
   Demonstration of the online Monitor 


----------------------------------------------------------------
Ngrok service
----------------------------------------------------------------

The project supports use of the Ngrok_ service to test application of the 
servers in the ``bin/`` folder online.

.. _Ngrok: https://ngrok.com/


The Ngrok_ provides secure tunnels to the localhost and temporary domain names to
access your developing server outside the NAT.

To start the ngrok service, simply do

.. code-block:: 
    
     cd bin;
   ./setup.daemons.sh;
   export DAEMON_NAME=NGROK;   
   ./daemon.sh start;

Then if you check the file ``ngrok.log`` file, you will find the domain name assigned by the Ngrok_, e.g.
``http://7b5e1384.ngrok.com``.You can use ``7b5e1384.ngrok.com`` later in the 
``templates/monitor_multi_transform_socketio.html`` to get access to online motinor server even on all
computers or mobile devices connected to the internet.


----------------------------------------------------------------
Documentation
----------------------------------------------------------------

The main parts of documentation are placed  in the ``README.rst``.
The python and shell codes in ``bin/`` and ``lib/`` folders are documented as well.
The docstring are used for this purpose.
The ``Pylit`` frameworks and  two scripts: ``source_doc/create_documentation.sh`` and
``source_doc/shell2rst.py`` can be used to extract the docstrings and create 
the documentation of the source code as ``source_doc/{bin,lib}/*.rst`` files.

Running the command 

.. code-block:: 
    
    make doc
    
    
will make a folder ``doc/`` with the ``README.html`` which collects all ``*.rst`` files together in
the HTML format.

----------------------------------------------------------------
Samples
----------------------------------------------------------------



The file ``data/FL_insurance_sample.csv`` contains the samples used to create the pandas dataframe.

The file was download from spatialkey.com_.


.. _spatialkey.com: https://support.spatialkey.com/spatialkey-sample-csv-data/



----------------------------------------------------------------
References
----------------------------------------------------------------


.. target-notes::
    


-----------------------------------------
 Documentation of the source code 
-----------------------------------------

Here will be the documentation of the code from 
``source_doc/{bin,lib}/*rst`` inserted.

.. include:: source_doc/bin/monitorserver_multi_transform.py.rst
.. include:: source_doc/bin/monitorserver.py.rst
.. include:: source_doc/lib/Logger.py.rst
.. include:: source_doc/lib/pandas_transform.py.rst
.. include:: source_doc/lib/settings.py.rst
.. include:: source_doc/bin/respawn_double_fork.sh.rst
.. include:: source_doc/bin/setup.daemons.sh.rst
.. include:: source_doc/bin/run_ngrok.sh.rst
.. include:: source_doc/bin/daemon.sh.rst