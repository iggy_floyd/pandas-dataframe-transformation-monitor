#!/usr/bin/env bash

# Daemon launcher of the Ngrok service
# ==========================================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   run_ngrok.sh
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

# The script initialize daemons which will be operated by ``daemon.sh``
# 

# Usage
# -------------------------------
#
# ./run_ngrok.sh <port>


# The shell code 
# -----------------------
#
# ::

`pwd`/ngrok -log=ngrok.log $1 
