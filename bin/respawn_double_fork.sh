#!/bin/bash

# Respawn  service
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   respawn_double_fork.sh
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

# This script is a simple respawn deamon for those of us who don't want
# to deal with the /etc/event.d, monitor etc...
# 

# Usage
# -------------------------------
#
# Usage: ( respawn_double_fork.sh [program] [sleep time] [search pattern] [killword] &) &


# The code of the service
# -----------------------
#
# ::

function run() {

[ $#  -lt 4 ] && echo "


This script is started  as a double-forked process! 
			|
			V

Usage: ( $0 [program] [sleep time] [search pattern] [killword] & ) &


It becomes the child of the init process. Because of the internal loop, it never dies!


It uses netcat to listen the port 9760 on localhost.


To kill it, please send  a message  via netcat:

echo -ne 'killmonitor' | nc -q1  localhost 9760


It sends the  message to the port 9761 on localhost. You can write a simple listener of the port:

while [ true ]; do nc -l -w1  -p 9761 2>/dev/null; done

" && return 1




[ !  `which nc` ] && return 1;


exec 3>&2
exec 4>&1
exec 2> /dev/null
exec 1> /dev/null

while [ true ]
do
    local request=`nc -l -w$2  -p 9760 2>/dev/null`
    if [ "$request" == "$4" ]
    then
        echo  "killing  $1...." | nc -q1  localhost 9761 2> /dev/null
        ps aux | grep -v grep | grep -v "$0" | grep  "$3" > /dev/null
        if [ $? -eq 0  ] 
        then
            ps aux | grep -v grep | grep -v "$0" | grep  "$3"  | awk '{print $2}' | xargs -I {} kill {}  2> /dev/null 1> /dev/null;
            echo  "$1 ...killed" | nc -q1  localhost 9761 2> /dev/null
        fi
        exec 2>&3
        exec 3>&-
        exec 1>&4
        exec 4>&-
        return 0;
    fi
    ps aux | grep -v grep | grep -v "$0" | grep  "$3" > /dev/null
    if [ $? -eq 1  ]
    then
        echo  "$1 : Stopped. Restarting in $2 seconds." | nc -q1  localhost 9761 2> /dev/null
        sleep $2
        $1 &  
        echo  "$1 : Restarted " | nc -q1  localhost 9761 2> /dev/null
    fi
done
}

run "$@"
 