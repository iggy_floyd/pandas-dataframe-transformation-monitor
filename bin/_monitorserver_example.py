from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
import pandas_transform as pdt

from flask import Flask,request, current_app
from flask import Flask, render_template
from flask.ext.socketio import SocketIO,emit,disconnect
from threading import Thread
import time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@socketio.on('client',namespace='/test')
def handle_message(message):
    print('received message: ' + str(message))
    emit('server', 'Server got your message %r'%message,namespace='/test')

thread=None

def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        time.sleep(3)
        count += 1
        socketio.emit('server', 'Server got your message %d'%count,   namespace='/test')
        print "count %d"%count

@socketio.on('start', namespace='/test')
def start():
    global thread
    if thread is None:
        thread = Thread(target=background_thread)
        thread.daemon =True
        print "thread started"
        thread.start()

if __name__ == '__main__':
    socketio.run(app)
