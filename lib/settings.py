#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Settings
# ==============================
#
# :Date: Sep 28, 2015, 12:59:09 PM
# :File:   settings.py
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#


'''
   settings of the different classes
'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Sep 29, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"

# Requirements
# ------------
# ::


from os import sys, path
import logging



# ``data_path``
# ------------------------
# path to the data folder
# ::


data_path=path.dirname(path.dirname(path.abspath(__file__)))+"/data/"


# `verbosity level`
# ---------------------
# path to the log folder and verbosity level
# ::

log_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/log/"
log_level= logging.ERROR # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
#log_level= logging.NOTSET # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
mylogging_threshold=7 # debug level by default

