..  #! /usr/bin/env python
  # -*- coding: utf-8 -*-
  
  
pandas_transform
==================================================================

:Date: Sep 28, 2015, 10:53:42 AM
:File: pandas_transform.py
:Copyright:  @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de





::

  '''
      pandas_transform ... 
  
  '''
  
  
Authors and Version
------------------------




::

  __author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
  __date__ ="$Sep 28, 2015 10:53:42 AM$"
  __docformat__ = 'restructuredtext'
  __version__ = "0.0.1"
  
  
Requirements
------------





::

  import sys
  sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path 
  import json
  import numpy as np
  import pandas as pd
  import random
  import threading
  from blinker import Signal
  from settings import data_path
  from Logger import mylogging
  from time import sleep
  
  
Dataframe and other parameters of the transformations
------------------------------------------------------------------------



::

  dataframe=pd.read_csv(data_path+'FL_insurance_sample.csv')
  constructions = pd.Series(dataframe['construction'].values.ravel()).unique();
  slowness = 0.005 #5ms
  
  
function ``print_percent(a, b)``
------------------------------------------------------------------------
prints the percentage of the progress to sys.stdout




::

  def print_percent(a, b):
      '''prints the percentage of the progress to sys.stdout'''
      percent = ((a * 100) / b)
      sys.stdout.write('\033[D \033[D' * 4 + format(percent, '3.0f') + '%')
      sys.stdout.flush()
  
  
class ``RepeatedTimer``
------------------------------------------------------------------------
the class responsible for repeating some function in time (per 1 s second)




::

  class RepeatedTimer:
      
      '''
      the class responsible for repeating some function in time (per 1 s second), for example
      >timer = RepeatedTimer(1, lambda: print_percent(total, BIGFILE_SIZE))
      >timer.start()
      >try:
      >    while 1:
      >        f.write(chunk)
      >        total += chunk_len
      >        if total >= BIGFILE_SIZE:
      >            break
      >finally:
      >    f.close()
      >    timer.stop()
      '''
  
      def __init__(self, timeout, fun):
          self.timeout = timeout
          self.fun = fun
  
      def start(self):
          
          def main():
              self.fun()
              self.start()
  
          self.timer = threading.Timer(1, main)
          self.timer.start()
  
  
      def stop(self):
          self.timer.cancel()
  
  
function ``logged_apply(g, func, *args, **kwargs)``
------------------------------------------------------------------------
makes the apply function to be logged




::

  def logged_apply(g, func, *args, **kwargs):
      '''makes the apply function to be logged'''
      
      step_percentage = 100. / len(g)
      sys.stdout.write('apply progress:   0%')
      sys.stdout.flush()
  
function ``logging_decorator(func)``
``````````````````````````````````````````````````````
decorates the function such that it would be possible to log the process completeness


    
::

      def logging_decorator(func):
          '''decorates the function such that it would be possible to log the process completeness'''
          
          def wrapper(*args, **kwargs):
              '''wrapper'''
              wrapper.count += 1
              return func(*args, **kwargs)
          wrapper.count = 0
          return wrapper
  
      logged_func = logging_decorator(func)
      
      timer = RepeatedTimer(1, lambda: print_percent(logged_func.count, float(len(g))))
      timer.start()
      res = g.apply(logged_func,axis=1)
      timer.stop()
      print_percent(float(len(g)),float(len(g)))
      return res
  
  
function ``pandas_transform_stdout(dataframe=dataframe)``
------------------------------------------------------------------------
performs the transformation of a dataframe and and prints the progress on sys.stdout



::

  def pandas_transform_stdout(dataframe=dataframe):
      ''' performs the transformation of a dataframe 
          and prints the progress on sys.stdout
      '''
      
      def transform_func(x):
          '''makes slow transformations'''
                  
          sleep(slowness); # sleep each 'slowness' fraction of the second
          return x['construction']+'_transformed'
      
      dataframe['construction_transformed']=logged_apply(dataframe, transform_func)
      mylogging(dataframe['construction_transformed'].head())
      return
  
  
class ``Processor_pandas_transform``
------------------------------------------------------------------------
makes transformation of the dataframe and sends signals at different stages of the transformation




::

  class Processor_pandas_transform(object):
      ''' makes transformation of the dataframe and sends signals at different stages of the transformation '''
      on_ready = Signal()
      on_process = Signal()
      on_complete = Signal()
  
      
      def __init__(self, name,dataframe):
          self.name = name
          self.dataframe = dataframe
  
      def go(self):
          Processor_pandas_transform.on_ready.send(self.name)
          
          def logged_apply(g, func, *args, **kwargs):
              def logging_decorator(func):
                  def wrapper(*args, **kwargs):
                      wrapper.count += 1
                      Processor_pandas_transform.on_process.send(self.name,count=wrapper.count,total=float(len(g)))                    
                      return func(*args, **kwargs)
                  wrapper.count = 0
                  return wrapper
              
              logged_func = logging_decorator(func)            
              res = g.apply(logged_func,axis=1)
              return res
          
          def transform_func(x):
              '''makes slow transformations'''
                  
              sleep(0.005); # sleep each 5ms                        
              return (x['construction']+'_transformed',x['tiv_2011']+100.*np.random.normal())
              
          
          self.dataframe['construction_transformed'],self.dataframe['tiv_2011_transformed']=zip(*logged_apply(self.dataframe,transform_func))
          Processor_pandas_transform.on_complete.send(self.name,dataframe=self.dataframe)
  
      def __repr__(self):
          return '<Processor %s>' % self.name
  
  
functions ``funcX``
------------------------------------------------------------------------
performs different transformations




::

  def func1(x):
      '''transformation of construction and tiv_2012'''
      sleep(0.005); # sleep each 5ms                        
      return (x['construction']+'_transformed',x['tiv_2011']+100.*np.random.normal())
  
  
  def func2(x):
      '''tiv_2012 transformation'''
      sleep(0.005); # sleep each 5ms                        
      return (x['tiv_2012']+100.*np.random.normal(),)
  
  
  def func3(x):
      '''site_limit transformation'''
      sleep(0.005); # sleep each 5ms                        
      return (x['fl_site_limit']+100.*np.random.normal() ,x['hu_site_limit']+100.*np.random.normal(),x['eq_site_limit']+100.*np.random.normal())
  
list ``transformations``
------------------------------------------------------------------------
assign output of the transformation functions to names of new columns which will be created and filled in the datatframe





::

  transformations=[
      (func1,('construction_transformed','tiv_2011_transformed')),
      (func2,('tiv_2012_transformed',)),
      (func3,('fl_site_limit_transformed','hu_site_limit_transformed','eq_site_limit_transformed'))
  ]
  
class ``Processor_pandas_multi_transform``
------------------------------------------------------------------------
makes many transformations  of the dataframe and sends signals at different stages of the transformation.
It performs serveral transformation defined by ``transformations`` list.





::

  class Processor_pandas_multi_transform(object):
      '''makes many transformations  of the dataframe and sends signals at different stages of the transformation 
         It performs serveral transformation defined by ``transformations`` list.
      '''
      on_ready = Signal()
      on_total_process = Signal()
      on_current_process = Signal()
      on_complete = Signal()
  
      
      def __init__(self, name,dataframe):
          self.name = name
          self.dataframe = dataframe
  
      def go(self):
          global transformations
          Processor_pandas_multi_transform.on_ready.send(self.name)
          
          def logged_apply(g, func, *args, **kwargs):
              def logging_decorator(func):
                  def wrapper(*args, **kwargs):
                      wrapper.count += 1
                      Processor_pandas_multi_transform.on_current_process.send(self.name,title=func.__doc__,count=wrapper.count,total=float(len(g)))                    
                      return func(*args, **kwargs)
                  wrapper.count = 0
                  return wrapper
              
              logged_func = logging_decorator(func)            
              res = g.apply(logged_func,axis=1)
              return res
          
  
          # this part makes multi transformations and corresponding  assignments!
          total_transforms=len(transformations)        
          for i,transform in enumerate(transformations):
              func,labels = transform
              result=logged_apply(self.dataframe,func)            
              unzipped_result=zip(*result)
              for j,label in enumerate(labels):
                  self.dataframe[label] = unzipped_result[j]
              Processor_pandas_multi_transform.on_total_process.send(self.name,count=float(i+1),total=float(total_transforms))
              
          
          Processor_pandas_multi_transform.on_complete.send(self.name,dataframe=self.dataframe)
  
      def __repr__(self):
          return '<Processor %s>' % self.name
  
  
  
  
  
The main entrance point of the pandas_transform.py
----------------------------------------------------------------

 .. code-block:: bash

                 ~/project/bin> python pandas_transform.py




::

  if __name__ == "__main__":
      
      # test Nr.1
      mylogging("test 1: printing the progress bar using RepeatedTimer")
      pandas_transform_stdout(dataframe.loc[:1000])
  
  
  
      # test Nr.2
      mylogging("test 2: printing the progress bar using signals")
      pt=Processor_pandas_transform('Pandas_Transformer',dataframe.loc[:1000])
      mylogging(pt)
      
      @pt.on_complete.connect
      def completed(sender_name,**data):
          mylogging ("Processor %s completed! %s" %(sender_name,data["dataframe"].head()))
          
      @pt.on_ready.connect
      def ready(sender_name):
          mylogging ("Processor %s is ready to start!" % sender_name)        
          
      @pt.on_process.connect
      def process(sender_name,**data):
          print_percent(data["count"],data["total"])        
          
      # 3...2...1... GO!            
      pt.go()
      
      
      # test Nr.3
      mylogging("test 3: multi transformations and printing the progress bar using signals")
      mpt=Processor_pandas_multi_transform('Pandas_Multi_Transformer',dataframe.loc[:1000])
      mylogging(mpt)
      
      
      
      @mpt.on_complete.connect
      def completed(sender_name,**data):
          mylogging ("Processor %s completed! %s" %(sender_name,data["dataframe"].head()))
          
      @mpt.on_ready.connect
      def ready(sender_name):
          mylogging ("Processor %s is ready to start!" % sender_name)    
          
  
      old_title = None
      @mpt.on_current_process.connect
      def current_process(sender_name,**data):
          global old_title
          if (old_title is None) or (old_title != data['title']):
              sys.stdout.write('apply progress of %s:   0%%'%data['title'])
              sys.stdout.flush()
              old_title = data['title']            
          print_percent(data["count"],data["total"])        
              
      @mpt.on_total_process.connect
      def total_process(sender_name,**data):        
          print 
          print ("total done %f"%(100.*data["count"]/data["total"]))
          
      # 3...2...1... GO!            
      mpt.go()
