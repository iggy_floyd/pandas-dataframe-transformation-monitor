   


Setup of the daemons 
============================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   setup.daemons.sh 
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de    


The script initialize daemons which will be operated by ``daemon.sh`` 
    


Usage 
------------------------------- 
 
./setup.daemons.sh    
   


OOBash -  oo-style framework for bash 4 
----------------------------------------------------- 
 
::    
   




   source _oobash.sh 
   


Make sure to delete all objects at end of program 



   trap 'delete_all' TERM EXIT INT
   
   


The shell code  
----------------------- 
 
::    
   




   _setuppath=`pwd`
   




   declare -A settings # please note, that associative array can be created in the bash with version >4.0
   




   _config_ini="${_setuppath}/config.ini"
   if [ -f "$_config_ini" ] 
   then
       source ${_config_ini}
   fi
   
   
   




   [ ! ${settings[MONITORSOCKETIO]+abc} ] && {
       echo "Hi";
       new DAEMON daemon1 "MONITORSOCKETIO" "`pwd`/monitorserver.py";
       $daemon1.store settings;
       }
   
   




   [ ! ${settings[MONITORSOCKETIO_MULTI_TRANSFORM]+abc} ] && 
   {
      new DAEMON daemon2 "MONITORSOCKETIO_MULTI_TRANSFORM" "`pwd`/monitorserver_multi_transform.py"
       $daemon2.store settings
   }
   




   [ ! ${settings[NGROK]+abc} ] && 
   {
      new DAEMON daemon3 "NGROK" "`pwd`/run_ngrok.sh 5000"
       $daemon3.store settings
   }
   
   
   
   




   [ ! ${settings[FAKE]+abc} ] && 
   {
      new DAEMON daemon4 "FAKE" "`pwd`/fake.sh"
      $daemon4.store settings
   }
   
   




   declare -p  settings > ${_config_ini};