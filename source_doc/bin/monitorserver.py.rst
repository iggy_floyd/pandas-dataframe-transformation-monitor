..  #!/usr/bin/env python
  
  from os import sys, path
  sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
  import pandas_transform as pdt
  from Logger import mylogging 
  
  from flask import Flask,request, current_app
  from flask import Flask, render_template
  from flask.ext.socketio import SocketIO,emit,disconnect
  from threading import Thread
  import time
  
  app = Flask(__name__)
  app.config['SECRET_KEY'] = 'secret!'
  socketio = SocketIO(app)
  
  
  
  # transformer
  pt=pdt.Processor_pandas_transform('Pandas_Transformer',pdt.dataframe.loc[:1000])
  
  
  @pt.on_ready.connect
  def ready(sender_name):
      mylogging ("Processor %s is ready to start!" % sender_name)   
      socketio.emit('server', 'Server is starting to transform...',   namespace='/test')
      
  
  
  @pt.on_complete.connect
  def completed(sender_name,**data):
      global thread
      mylogging ("Processor %s completed! %s" %(sender_name,data["dataframe"].head()))
      socketio.emit('server', 'Server has completed.',   namespace='/test')    
      thread=None
  
  
  @pt.on_process.connect
  def process(sender_name,**data):
      pdt.print_percent(data["count"],data["total"])            
      socketio.emit('progress', '%f'%(100.*float(data["count"])/data["total"]),   namespace='/test')
      
          
  
  
  @socketio.on('client',namespace='/test')
  def handle_message(message):
      mylogging ('received message: ' + str(message))    
      emit('server', 'Server got your message %r'%message,namespace='/test')
  
  thread=None
  def background_thread():
      """Example of how to send server generated events to clients."""
      pt.go()
      
  
  
  @socketio.on('transform', namespace='/test')
  def transform():
      global thread
      if thread is None:
          thread = Thread(target=background_thread)
          thread.daemon =True
          mylogging ( "starting thread")
  
          thread.start()
  
  
  @socketio.on('disconnect', namespace='/test')
  def test_disconnect():
      mylogging ('Client disconnected')  
      
  
  if __name__ == '__main__':
      socketio.run(app)
