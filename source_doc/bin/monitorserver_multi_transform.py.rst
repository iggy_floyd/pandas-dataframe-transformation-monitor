..  #!/usr/bin/env python
  
-*- coding: utf-8 -*-

Monitor Server of the Mutli-Task Pandas DataFrame Transformer
==================================================================

:Date: Sep 24, 2015, 9:28:51 AM
:File:   monitorserver_multi_transform.py
:Copyright:  @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de



The server starts transformation of the pandas dataframe in the background.
Then it opens and listens to the web socket to transfer the status of the background process.



::

  '''
      The  demo server transforming a pandas dataframe and sending notification to open web sockets.
  '''
  
  
Authors and Version
------------------------

::



  __author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
  __date__ ="$Sep 24, 2015 9:28:51 AM$"
  __docformat__ = 'restructuredtext'
  __version__ = "0.0.1"
  
  
  
Requirements
------------

::

  from os import sys, path
  sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
  import pandas_transform as pdt
  from Logger import mylogging 
  
  from flask import Flask,request, current_app
  from flask import Flask, render_template
  from flask.ext.socketio import SocketIO,emit,disconnect
  from threading import Thread
  import time
  
  
Flask server application and Socket.IO support
-----------------------------------------------
::

  app = Flask(__name__)
  app.config['SECRET_KEY'] = 'secret!'
  socketio = SocketIO(app)
  
  
Multi-taks pandas dataframe transformer
--------------------------------------------
::

  # multi-taks transformer
  mpt=pdt.Processor_pandas_multi_transform('Pandas_Multi_Transformer',pdt.dataframe.loc[:1000])
  
  
``blinker`` Signal callbacks and socket.io emiters
---------------------------------------------------
::

  @mpt.on_ready.connect
  def ready(sender_name):
      mylogging ("Processor %s is ready to start!" % sender_name)   
      socketio.emit('server', 'Server is starting to transform...',   namespace='/test')
      
  
  
  @mpt.on_complete.connect
  def completed(sender_name,**data):
      global thread
      mylogging ("Processor %s completed! %s" %(sender_name,data["dataframe"].head()))
      socketio.emit('server', 'Processor %s completed!'%(sender_name),   namespace='/test')    
      thread=None
  
  
  @mpt.on_total_process.connect
  def total_process(sender_name,**data):
      pdt.print_percent(data["count"],data["total"])            
      socketio.emit('progress', '%f'%(100.*float(data["count"])/data["total"]),   namespace='/test')
  
  @mpt.on_current_process.connect
  def current_process(sender_name,**data):
      pdt.print_percent(data["count"],data["total"])
      data={
          'title':data['title'],
          'percentage':100.*float(data["count"])/data["total"],
      }    
      socketio.emit('progress_current', data,   namespace='/test')
  
      
  
  @socketio.on('client',namespace='/test')
  def handle_message(message):
      print('received message: ' + str(message))
      emit('server', 'Server got your message %r'%message,namespace='/test')
  
  
  thread=None
  def background_thread():
      """Example of how to send server generated events to clients."""
      mpt.go()
      
  
  
  @socketio.on('transform', namespace='/test')
  def transform():
      global thread
      if thread is None:
          thread = Thread(target=background_thread)
          thread.daemon =True
          print "thread started"
          emit('server', 'Server: Thread started',namespace='/test')
          thread.start()
  
  
  @socketio.on('disconnect', namespace='/test')
  def test_disconnect():
      print('Client disconnected')
  
  
  
  
The main entrance point of the ``monitorserver_multi_transform.py``
----------------------------------------------------------------------------

 .. code-block:: bash

                 ~/project/bin> python monitorserver_multi_transform.py


::


  if __name__ == '__main__':
      socketio.run(app)
  
