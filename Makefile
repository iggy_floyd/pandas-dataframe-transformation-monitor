# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project

description = 'An example of the monitor for pandas dataframe transformations'

all: configuration doc


# to check that the system has all needed components
configuration: configure
	-@  mkdir log 2>/dev/null
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.rst
	-@mkdir doc 2>/dev/null
	-@cd source_doc/; ./create_documentation.sh `ls ../bin/*.py` `ls ../lib/*.py` `ls ../bin/*.sh` 
	-@rst-tool/create_docs.sh README.rst `basename $(PWD)` $(description); mv README.html doc;
	

test: 
	-@echo "Not implemented"



run: 
	-@echo "Not implemented"


build: 
	-@echo "Test suites:"


# to clean all temporary stuff
clean:  
	-@rm -r config.log autom4te.cache
	-@rm -r doc log/logs
	-@rm README



serve:
	-@firefox localhost:8010 & 
	-@python -m SimpleHTTPServer 8010



.PHONY: configuration clean all doc run test  clean serve test build
