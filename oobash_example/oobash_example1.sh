source oobash1.sh

## class Person
# Definition der Methoden funktionen, die in der Metatable
# dann in Methoden umgewandelt werden.
Person_new()
{
    local self=$1
    shift

    $self.set_name "$1"
}


Person_delete()
{
    echo "Destructor for $($1.name) called"
}


Person_say_hello()
{
    # Get self reference.
    local self=$1

    shift
    
    echo "$self: Hello my name is $($self.name)"
}


# Class metatable
Person=(
    # Constructor
    function __new__ Person_new

    function __delete__ Person_delete

    # Methoden-deklaration: say_hello
    function say_hello Person_say_hello

    # member variable: name
    # Die inneren Hochkommata sind wichtig!
    # Für jede member-variable wird je ein getter
    # und ein setter generiert.
    var name "'Default value'"
)

## end of class Person


# "Instantiierung" zweier Objekte der "Klasse" Person
# Hierdurch wird die Methode __new__ des Objektes (also die Funktion Person_new)
# mit
# einer "Referenz" des Objektes und den mitgegebenen Argumenten aufgerufen.
new Person person "Dominik"
new Person person2 "Max Mustermann"

# Hier wird jeweils die verknüpfte Funktion aufgerufen,
# mit einer "Referenz" auf das Objekt als erstes Argument.
# Alle restlichen Argumente werden hinten angestellt.
$person.say_hello
$person2.say_hello

# Neuen Namen setzen
$person.set_name "Fritz Nochwas"

$person.say_hello

echo "Der Name von \$person ist: $($person.name)"
# Alle angelegten Objekte werden "zerstört".
# Sprich alle Destruktoren werden aufgerufen (__delete__-methoden)
delete_all
