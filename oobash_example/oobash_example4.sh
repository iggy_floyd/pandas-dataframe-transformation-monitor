#!/bin/bash

source _oobash.sh

## declare settings array
declare -A settings # please note, that associative array can be created in the bash with version >4.0

_setuppath=`pwd`

## config.ini stores previous settings 
_config_ini="${_setuppath}/config.ini"
if [ -f "$_config_ini" ] 
then
    source ${_config_ini}
fi

## class DAEMON

DAEMON=(
    # Constructor.
    # No explicit definition so DAEMON::__new__() is used.
    function __new__

    # Destructor
    function __delete__

    function get_name 
    function get_uuid
    function get_cmd
    function store
    function restore_cmd
    function restore_uuid

    declare name
    declare uuid 
    declare cmd
)

## Tempfile implementation

DAEMON::__new__(){
    local self=$1
    shift

    $self.set_name "$1"
    $self.set_uuid "$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1)"
    $self.set_cmd "$2"
}


DAEMON::__delete__(){
    echo "that's all";
}


DAEMON::get_name()
{
# Get self reference.
    local self=$1
    shift    
    echo "$($self.name)"
}


DAEMON::get_uuid()
{
# Get self reference.
    local self=$1
    shift    
    echo "$($self.uuid)"
}



DAEMON::get_cmd()
{
# Get self reference.
    local self=$1
    shift 
    echo "$($self.cmd)"       
}


DAEMON::store()
{
# Get self reference.
    local self=$1
    shift 
    eval "$1[$($self.name)]=\"$($self.uuid) $($self.cmd)\""
}


DAEMON::restore_cmd()
{
# Get self reference.
    local self=$1
    shift     
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);      
    echo "${_arr[@]:1:${#_arr}}"    
}

DAEMON::restore_uuid()
{
# Get self reference.
    local self=$1
    shift     
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);      
    echo "${_arr[@]:0:1}"    
}


# Make sure to delete all objects at end of program
trap 'delete_all' TERM EXIT INT


new DAEMON daemon1 "MONITORSOCKETIO" "`pwd`/monitorserver_multi_transform.py --asd --asd"

#echo $($daemon1.get_name)
#echo $($daemon1.get_uuid)
#echo $($daemon1.get_cmd)



$daemon1.store settings
$daemon1.restore_cmd settings
$daemon1.restore_uuid settings

## store settings to the file
declare -p  settings > ${_config_ini};

