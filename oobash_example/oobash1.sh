declare -A __OOB_OBJ_TABLE
declare -A __OOB_ADDR_TABLE


# Create new object without registering it in the object table.

# Arguments:
#    * unique objectname
#    * variable name to export object name to
#    * member declarations ...
new_metaobject()
{
    local OBJ_NAME=$1
    shift

    local VARNAME=$1
    shift

    while ! [[ -z $1 ]]; do
        if [[ $1 == "function" ]]; then
            eval "$OBJ_NAME.$2() { $3 $OBJ_NAME \"\$@\"; }"
        elif [[ $1 == "var" ]]; then
            eval ${OBJ_NAME}_$2=$3
            eval "$OBJ_NAME.$2() { echo \"\$${OBJ_NAME}_$2\"; }"
            eval "$OBJ_NAME.set_$2() { ${OBJ_NAME}_$2=\$1; }"
        fi
        shift 3
    done

    eval $VARNAME=$OBJ_NAME
}


# Create new object and register it in the object table

# Arguments:
#    * Variable name of the class array
#    * Variable name to export object name to
#    + (optional) arguments to constructor.
new()
{
    local CLASS=$1
    shift

    local VARNAME=$1
    shift

    local i

    # Increment class id number.
    if [[ -z ${__OOB_OBJ_TABLE[$CLASS]} ]]; then
        __OOB_OBJ_TABLE[$CLASS]=1
    else
        ((__OOB_OBJ_TABLE[$CLASS] += 1))
    fi


    # Generate unique object-name.
    local OBJ_NAME="${CLASS}_Object_id_${__OOB_OBJ_TABLE[$CLASS]}"

    # Register object-name.
    __OOB_ADDR_TABLE[$CLASS]="${__OOB_ADDR_TABLE[$CLASS]}:$OBJ_NAME:"


    # Create new object.
    eval new_metaobject $OBJ_NAME $VARNAME \"\${$CLASS[@]}\"

    # Call constructor.
    [[ $(type -t $OBJ_NAME.__new__) == function ]] && $OBJ_NAME.__new__ "$@"
}


# Deletes All references to the object
# and calls the destructor if it exists.

# Arguments:
#    * A reference to an object
delete()
{
    local CLASSNAME=$(echo $1|sed -r 's/_Object_id_[0-9]+$//')

    __OOB_ADDR_TABLE[$CLASSNAME]=$(echo "${__OOB_ADDR_TABLE[$CLASSNAME]}"|sed -r "s/:$1://")

    if [[ -z ${__OOB_ADDR_TABLE[$CLASSNAME]} ]]; then
        unset __OOB_ADDR_TABLE[$CLASSNAME]
    fi

    # Check for destructor and call it if one is existent.
    [[ $(type -t $1.__delete__) == function ]] && $1.__delete__
}


# Deletes all references to the objects of all or
# specific classes.

# Arguments:
#    + (optional) Classnames ...
delete_all()
{
    local i
    local j

    if [[ -z $1 ]]; then
        # Loop through all registered objects and delete them
        for i in "${__OOB_ADDR_TABLE[@]}"; do
            local a=$(echo "$i"| \
                awk 'BEGIN { RS = ":+" } /^.+$/ {print $1" "}')

            for j in $a; do
                delete $j
            done
        done
    else
        for i; do
            local str=${__OOB_ADDR_TABLE[$i]}
            local a=$(echo "$str"| \
                awk 'BEGIN { RS = ":+" } /^.+$/ {print $1" "}')

            for j in $a; do
                delete $j
            done
        done
    fi
}
