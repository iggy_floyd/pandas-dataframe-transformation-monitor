#!/bin/bash

source _oobash.sh


## class File

File=(
    function write = File::write  # Explicit to make inheritance possible.

    # No default value
    declare name
)

## File implementation

File::write()
{
    local self=$1
    shift

    echo "$1" >>"$($self.name)"
}


## class Tempfile

Tempfile=("${File[@]}"  # Inherits File
    # Constructor.
    # No explicit definition so Tempfile::__new__() is used.
    function __new__

    # Destructor.
    function __delete__

    # Specifies whether the file should be deleted after
    # the destruction of the object.
    # Take notice about the default value set with =
    declare delete = 1
)

## Tempfile implementation

Tempfile::__new__()
{
    local self=$1
    shift

    $self.set_name "$(mktemp)"

    [[ -z $1 ]] || $self.set_delete "$1"
}


Tempfile::__delete__()
{
    local self=$1
    shift

    [[ $($self.delete) == 1 ]] && rm -f "$($self.name)"
}


# Make sure to delete all objects at end of program
trap 'delete_all' TERM EXIT INT

new Tempfile tmp

$tmp.write "'Hey dudes'"

cat $($tmp.name)

$tmp.delete
echo "\$tmp.name: $($tmp.name)"
